use crate::udp::NonQuicUdpSocket;

use anyhow::{bail, Result};
use quinn::{default_runtime, ClientConfig, Endpoint, EndpointConfig, ServerConfig};
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use tokio::net::UdpSocket;

mod udp;

#[tokio::main]
async fn main() -> Result<()> {
    let args: Vec<_> = std::env::args().collect();
    if args.len() != 2 {
        bail!("Syntax error, expected: {} [--server|--client]", args[0]);
    }

    if args[1] == "--server" {
        server().await
    } else if args[1] == "--client" {
        client().await
    } else {
        bail!("Invalid parameter: {}", args[1]);
    }
}

async fn server() -> Result<()> {
    let names = vec!["localhost".into()];
    let cert = rcgen::generate_simple_self_signed(names)?;
    let cert_der = cert.serialize_der()?;
    let priv_key = cert.serialize_private_key_der();

    tokio::fs::write("tmp.cert", &cert_der).await?;

    let certificate = rustls::Certificate(cert_der);
    let private_key = rustls::PrivateKey(priv_key);

    let cert_chain = vec![certificate];

    let server_config = ServerConfig::with_single_cert(cert_chain, private_key)?;

    let bind_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::LOCALHOST), 1234);
    let udp_socket = UdpSocket::bind(bind_addr).await?;

    let (quic_socket, non_quic_socket) = udp::multiplex(udp_socket);

    let runtime = default_runtime()
        .ok_or_else(|| std::io::Error::new(std::io::ErrorKind::Other, "no async runtime found"))?;
    let endpoint = Endpoint::new_with_abstract_socket(
        EndpointConfig::default(),
        Some(server_config),
        quic_socket,
        runtime,
    )?;

    let connecting = endpoint.accept().await.unwrap();
    let conn = connecting.await?;

    tokio::spawn(async move {
        if let Err(err) = recv_non_quic(non_quic_socket).await {
            eprintln!("recv_non_quic() error: {err}");
        }
    });

    let mut uni = conn.accept_uni().await?;
    let mut buf = [0; 4];
    loop {
        let _ = uni.read_exact(&mut buf).await?;
        let value = u32::from_be_bytes(buf);
        println!("received on QUIC stream: {value}");
    }
}

async fn recv_non_quic(mut non_quic_socket: NonQuicUdpSocket) -> Result<()> {
    let mut buf = vec![0; 2048];
    loop {
        let (size, addr) = non_quic_socket.recv_from(&mut buf).await?;
        println!("received on UDP a datagram of {size} bytes from {addr}");
    }
}

async fn client() -> Result<()> {
    let cert_der = tokio::fs::read("tmp.cert").await?;

    let certificate = rustls::Certificate(cert_der);
    let mut certs = rustls::RootCertStore::empty();
    certs.add(&certificate)?;

    let client_config = ClientConfig::with_root_certificates(certs);

    let bind_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::UNSPECIFIED), 5678);
    let udp_socket = UdpSocket::bind(bind_addr).await?;

    let (quic_socket, non_quic_socket) = udp::multiplex(udp_socket);

    let runtime = default_runtime()
        .ok_or_else(|| std::io::Error::new(std::io::ErrorKind::Other, "no async runtime found"))?;
    let endpoint = Endpoint::new_with_abstract_socket(
        EndpointConfig::default(),
        None,
        quic_socket,
        runtime,
    )?;

    let server_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::LOCALHOST), 1234);

    let conn = endpoint.connect_with(client_config, server_addr, "localhost")?.await?;

    tokio::spawn(async move {
        if let Err(err) = send_non_quic(non_quic_socket, server_addr).await {
            eprintln!("send_non_quic() error: {err}");
        }
    });

    let mut uni = conn.open_uni().await?;
    let mut value = 0u32;
    loop {
        uni.write_all(&value.to_be_bytes()).await?;
        println!("CLIENT: value {value} sent over QUIC stream");
        value = (value + 1) % 1000;

        tokio::time::sleep(tokio::time::Duration::from_millis(300)).await;
    }
}

async fn send_non_quic(non_quic_socket: NonQuicUdpSocket, target: SocketAddr) -> Result<()> {
    let mut buf = [0; 5];
    let mut value = 1000u32;
    loop {
        buf[1..].copy_from_slice(&value.to_be_bytes());
        let _size = non_quic_socket.send_to(&buf, target).await?;
        println!("CLIENT: value {value} sent over UDP");
        value += 1;
        if value == 2000 {
            value = 1000;
        }

        tokio::time::sleep(tokio::time::Duration::from_millis(430)).await;
    }
}
