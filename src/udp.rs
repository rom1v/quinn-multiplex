use std::io::{self, IoSliceMut};
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};

use bytes::Bytes;
use futures::ready;
use quinn_udp::{RecvMeta, Transmit, UdpState};
use tokio::net::ToSocketAddrs;
use tokio::sync::mpsc;

#[derive(Debug)]
pub struct QuicUdpSocket {
    socket: Arc<tokio::net::UdpSocket>,
    rx: Arc<Mutex<mpsc::UnboundedReceiver<Datagram>>>,
    quinn_inner: quinn_udp::UdpSocketState,
}

#[derive(Debug)]
pub struct NonQuicUdpSocket {
    socket: Arc<tokio::net::UdpSocket>,
    //rx: Arc<Mutex<mpsc::UnboundedReceiver<Datagram>>>,
    rx: mpsc::UnboundedReceiver<Datagram>,
}

pub struct Datagram {
    payload: Bytes,
    addr: SocketAddr,
}

impl Datagram {
    pub fn new(payload: Bytes, addr: SocketAddr) -> Self {
        Self { payload, addr }
    }
}

pub fn multiplex(socket: tokio::net::UdpSocket) -> (QuicUdpSocket, NonQuicUdpSocket) {
    let socket = Arc::new(socket);
    let (quic_tx, quic_rx) = mpsc::unbounded_channel();
    let (non_quic_tx, non_quic_rx) = mpsc::unbounded_channel();

    let socket2 = socket.clone();
    let _task = tokio::spawn(async move {
        if let Err(err) = task_dispatch(&socket2, quic_tx, non_quic_tx).await {
            eprintln!("Error receving datagrams: {err}");
        }
    });

    let quic_socket = QuicUdpSocket {
        socket: socket.clone(),
        rx: Arc::new(Mutex::new(quic_rx)),
        quinn_inner: quinn_udp::UdpSocketState::new(),
    };

    let non_quic_socket = NonQuicUdpSocket {
        socket: socket.clone(),
        //rx: Arc::new(Mutex::new(non_quic_rx)),
        rx: non_quic_rx,
    };

    (quic_socket, non_quic_socket)
}

async fn task_dispatch(
    socket: &tokio::net::UdpSocket,
    quic_tx: mpsc::UnboundedSender<Datagram>,
    non_quic_tx: mpsc::UnboundedSender<Datagram>,
) -> std::io::Result<()> {
    let mut buf = vec![0; 0x10000];
    loop {
        let (r, addr) = socket.recv_from(&mut buf).await?;
        let data = Bytes::copy_from_slice(&buf[..r]);
        let tx = if is_quic(&buf) {
            &quic_tx
        } else {
            &non_quic_tx
        };
        let datagram = Datagram::new(data, addr);
        let result = tx.send(datagram);
        if let Err(err) = result {
            eprintln!("Could not post datagram: {err}");
            // drop datagram
        }
    }
}

fn is_quic(payload: &[u8]) -> bool {
    if payload.is_empty() {
        false
    } else {
        let first = payload[0];
        (64..80).contains(&first) || (192..=255).contains(&first)
    }
}

impl NonQuicUdpSocket {
    pub async fn send_to<A: ToSocketAddrs>(&self, buf: &[u8], target: A) -> std::io::Result<usize> {
        self.socket.send_to(buf, target).await
    }

    pub async fn recv_from(&mut self, buf: &mut [u8]) -> std::io::Result<(usize, SocketAddr)> {
        let Datagram { payload, addr } = self
            .rx
            .recv()
            .await
            .ok_or_else(|| std::io::Error::new(std::io::ErrorKind::UnexpectedEof, "EOF"))?;
        let size = std::cmp::min(payload.len(), buf.len());
        buf[..size].copy_from_slice(&payload);
        Ok((size, addr))
    }
}

impl quinn::AsyncUdpSocket for QuicUdpSocket {
    fn poll_send(
        &self,
        state: &UdpState,
        cx: &mut Context,
        transmits: &[Transmit],
    ) -> Poll<Result<usize, io::Error>> {
        let inner = &self.quinn_inner;
        let io = &*self.socket;
        loop {
            ready!(io.poll_send_ready(cx))?;
            if let Ok(res) = io.try_io(tokio::io::Interest::WRITABLE, || {
                inner.send(io.into(), state, transmits)
            }) {
                return Poll::Ready(Ok(res));
            }
        }
    }

    fn poll_recv(
        &self,
        cx: &mut Context,
        bufs: &mut [IoSliceMut<'_>],
        meta: &mut [RecvMeta],
    ) -> Poll<io::Result<usize>> {
        // recv() is cancel-safe
        if let Some(Datagram { payload, addr }) = ready!(self.rx.lock().unwrap().poll_recv(cx)) {
            let mut offset = 0;
            for (buf, meta) in bufs.iter_mut().zip(meta) {
                assert!(offset <= payload.len());
                if offset == payload.len() {
                    // No more data to write
                    break;
                }

                let size = std::cmp::min(payload.len() - offset, buf.len());
                buf[..size].copy_from_slice(&payload[offset..offset + size]);
                offset += size;

                *meta = RecvMeta {
                    addr,
                    len: offset,
                    // sufficient?
                    ..Default::default()
                }
                //meta
            }
            Poll::Ready(Ok(offset))
        } else {
            Poll::Ready(Err(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "EOF",
            )))
        }
    }

    fn local_addr(&self) -> io::Result<SocketAddr> {
        self.socket.local_addr()
    }

    fn may_fragment(&self) -> bool {
        false
    }
}
